import { OrbitControls } from 'https://threejs.org/examples/jsm/controls/OrbitControls.js';

const { innerWidth, innerHeight } = window;
const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera(45, innerWidth/innerHeight, 0.1, 1000);
const renderer = new THREE.WebGLRenderer();

renderer.setSize(innerWidth, innerHeight);
document.querySelector('body').append(renderer.domElement);

const AxesHelper = new THREE.AxesHelper(10);
scene.add(AxesHelper);

const geometry = new THREE.SphereGeometry(1, 20, 20);

const material = new THREE.MeshBasicMaterial({
  color: '#3fbe83',
  wireframe: true,
});

const material2 = new THREE.MeshBasicMaterial({
  color: '#aaa',
  wireframe: true,
});

const earth = new THREE.Mesh(geometry, material);
const flatEarth = new THREE.Mesh(geometry, material2);
scene.add(earth);
scene.add(flatEarth);

camera.position.z = 10;
camera.position.x = 1;
camera.position.y = 1;

const controlsEarth = new OrbitControls(earth, renderer.domElement);
const controlsFlatEarth = new OrbitControls(flatEarth, renderer.domElement);

earth.position.y = 3;
flatEarth.scale.y = 0.1;

controlsEarth.target = new THREE.Vector3(4, 0, 0);
controlsEarth.autoRotate = true;
controlsEarth.autoRotateSpeed = 15;

controlsFlatEarth.target = new THREE.Vector3(4, 0, 0);
controlsFlatEarth.autoRotate = true;
controlsFlatEarth.autoRotateSpeed = 10;

const animate = () => {
  requestAnimationFrame(animate);
  controlsEarth.update();
  controlsFlatEarth.update();
  renderer.render(scene, camera);
};

animate();